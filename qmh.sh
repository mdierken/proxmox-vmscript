#!/usr/bin/env bash

if [[ ! -f /etc/vmscript ]]; then
    echo "VMScript is not installed"
    exit 1
fi

case $1 in
    ubuntu)
        if [[ ! -f /etc/vmscript/ubuntu/qmh_ubuntu.sh ]]; then
            echo "VMScript-Ubuntu is not installed!"
            exit 1
        fi

        /etc/vmscript/ubuntu/qmh_ubuntu.sh $2 $3 $4 $5

        ;;
    *)
        echo "Usage qmh <distribution:[ubuntu]> [options]"
        exit 1
        ;;
esac