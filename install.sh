#!/usr/bin/env bash

if [[ ! -f /etc/vmscript ]] ; then
    mkdir /etc/vmscript

    if [[ ! $? -eq 0 ]]; then
        echo "error creating /etc/vmscript, error code ${?}"
        exit 1
    fi
fi

function ubuntu {

    mkdir /etc/vmscript/ubuntu

    if [[ -z "$1" ]]; then
        START_ID=$1
    else
        START_ID=1
    fi

    echo -n START_ID > /etc/vmscript/ubuntu/cvmid_ubuntu
    cp qmh_ubuntu.sh /etc/vmscript/ubuntu/

}

HAS_INSTALL=0

case $1 in
    ubuntu)
        HAS_INSTALL=1
        ubuntu $2
        ;;
    *)
        echo "Usage: install.sh <distro name> [<vmid counter>]"
        ;;
esac

if [[ ${HAS_INSTALL} -eq 0 ]] ; then
    cp qmh.sh /etc/vmscript/
fi